# Synthase Domain Parser
## The Problem
Polyketide synthase (PKS) or nonribosomal peptide synthetase (NRPS) domain architecture predictions
from antiSMASH are convenient as a quick reference, but can be unreliable. NCBI's CD-search tool often
gives a fuller, more accurate picture, but manually clicking through each protein and writing
out domain architectures quickly becomes cumbersome when looking at genomes with many predicted
synthases. Therefore, a relatively automated way to parse CD-search results would be useful.

## A solution
This is a fairly basic script that takes the results of a CD-search run on a protein multi-FASTA file.
I wrote it to automate the process of getting the domain architecture of **all** PKS or NRPS in a fungal genome.
It looks for conserved domain hits corresponding to certain domain types, sorts them
by start position, filters out duplicates and overlapping domains, and does some further processing
based on the synthase type before printing the domain architecture to stdout. From my testing, it
seems to pull out the right domains in the right order the majority of the time.

The logic is fairly rudimentary - a domain is considered separate to the previous one if it's of
a different type, or if they're the same type but the start position is greater than the end position
of the previous hit (-10 to allow slight overlap). Overlapping domains of the same type are grouped,
and the biggest is used. Finally there's some renaming based on convention (e.g. DH to PT in a
non-reducing PKS, or ACP to T in a NRPS).

There is also a hacky little implementation of GenomeDiagram from BioPython to print visual representations
of these synthases to .svg and .pdf formats (works pretty well for a batch search with many synthases;
pretty broken for single searches... need to rewrite in pysvg).

## Usage
`python3 parse_cdsearch.py {pks/nrps} results [--visual synthases]`

Where `results` are a text file downloaded from a CD search run (Target Data: Domain hits, Data mode: Full, Align format: ASN text), and `synthases` is the fasta file
submitted to CD search (optional, ensure query names are identical and simple e.g. \>ABCD\_1234).

## Example
The MIBiG entry for the [cichorine PKS](https://mibig.secondarymetabolites.org/repository/BGC0000037/index.html#cluster-1) shows the following
domain architecture prediction underneath:

![cichorine PKS](images/cichorine_pks.png)

However, taking the amino acid sequence and submitting to CD search gives the following result:

![cichorine PKS CD-search result](images/cichorine_pks_cdsearch.png)

Downloading the results (Domain hits, Data mode: Full, ASN text) and using the script:

`python3 parse_cdsearch.py pks hitdata.txt`

Gives a tab delimited string:

`ANIA_06448	NRPKS	SAT-KS-AT-PT-ACP-ACP-CMT-TE`

## Bigger example
Use `extract_synthases.py` to pull out all of the predicted synthases from an antiSMASH GenBank file into
multiFASTA files (pks\_list.faa, nrps\_list.faa, terp\_list.faa). This picks up any protein that has
a 'sec\_met' qualifier with PKS/NRPS/terpene types.

`python3 extract_synthases.py scaffold_1.final.gbk`

Submit to batch CD-search, download the results and run the script:

`python3 parse_cdsearch.py pks hitdata.txt --visual pks_list.faa`

Which prints to stdout:

```
AHAN_008838	NRPKS	SAT-KS-AT-PT-ACP-TE
AHAN_010941	HRPKS	KS-AT-DH-ER-KR-ACP
AHAN_011164	PKS-NRPS	KS-AT-DH-CMT-KR-ACP-C-A-T-R
AHAN_011165	PKS-NRPS	KS-AT-DH-CMT-KR-ACP-C-A-T-R
AHAN_011367	NRPKS	SAT-KS-AT-PT-ACP-ACP-TE
AHAN_012076	NRPKS	SAT-KS-AT-PT-ACP-ACP-TE
AHAN_012149	NRPKS	SAT-KS-AT-PT-ACP-CMT-TR
AHAN_012626	HRPKS	KS-AT-DH-CMT-ER-KR-ACP
```

And writes a GenomeDiagram figure to .pdf and .svg that looks like:

![A. hancockii PKS](images/pks_domains.png)
