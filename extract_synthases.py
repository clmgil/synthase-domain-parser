"""
Extract synthases (PKS, NRPS, terpene) from antiSMASH .gbk.

Usage: python3 extract_synthases.py *.final.gbk

Cam Gilchrist
2018-06-05
"""

import sys, re, textwrap, argparse
from Bio import SeqIO

def build_fasta(gene_dict, out_file):
    """Given dict of synthases, write FASTA to file.
    Args:
        gene_dict (dict): Synthases, format locus:[description, sequence]
        out_file (str): Open file being written
    Returns:
        None"""

    for gene in gene_dict:
        cluster = gene_dict[gene][0]
        sequence = gene_dict[gene][1]

        print('{}\t{}'.format(gene, cluster))
        fasta = '>{}\n{}'.format(
                gene,
                # Limit to 80char per line
                textwrap.fill(sequence, width=80)
                )

        out_file.write('{}\n'.format(fasta))

def main(args):
    """Read antiSMASH summary GenBank file, write predicted synthases to FASTA.
    Args:
        args (parser): GenBank file
    Returns:
        None"""

    # Dictionaries to hold synthases
    pks = {}
    nrps = {}
    terp = {}

    # Regex pattern to extract cluster number
    bgc = re.compile(r'(Cluster_[0-9]*);')

    # Read in antiSMASH GenBank
    genome = SeqIO.parse(args.gbk, 'genbank')
    for scaf in genome:
        for feat in scaf.features:
            if feat.type == 'CDS':
                try:
                    # Will break, go to next CDS if no 'sec_met'
                    smtype = feat.qualifiers['sec_met'][0]
                    seq = feat.qualifiers['translation'][0]

                    try:
                        locus = feat.qualifiers['locus_tag'][0]
                    except:
                        locus = feat.qualifiers['gene'][0]
                        pass

                    # Only works if funannotate before antismash
                    try:
                        cluster = bgc.search(feat.qualifiers['note'][0]).group(1)
                    except:
                        cluster = 'no_bgc'
                        pass

                    # Easier just to check 'sec_met' qualifiers than have
                    # unique re patterns for everything...
                    # Also shouldn't miss any terpenes
                    if smtype in [
                            'Type: nrps',
                            'Type: terpene-nrps'
                            ]:
                        nrps[locus] = [cluster, seq]
                    elif smtype in [
                            'Type: nrps-t1pks',
                            'Type: t1pks',
                            'Type: t3pks'
                            ]:
                        pks[locus] = [cluster, seq]
                    elif smtype == 'Type: terpene':
                        terp[locus] = [cluster, seq]

                except Exception as e:
                    pass

    # Write FASTA files
    with open('pks_list.faa', 'w') as p:
        print('PKS')
        build_fasta(pks, p)

    with open('nrps_list.faa', 'w') as n:
        print('NRPS')
        build_fasta(nrps, n)

    with open('terp_list.faa', 'w') as t:
        print('Terpenes')
        build_fasta(terp, t)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
            description = 'Extract predicted synthases from antiSMASH GenBank.')
    parser.add_argument('gbk', help = 'antiSMASH GenBank file (*.final.gbk)')
    args = parser.parse_args()

    main(args)
